package edu.bo.examengrupal

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    var player = ArrayList<Int>()
    var cpu = ArrayList<Int>()
    var currentPlayer = 1

    fun cleanBoard()
    {
        b1.setBackgroundResource(android.R.drawable.btn_default)
        b2.setBackgroundResource(android.R.drawable.btn_default)
        b3.setBackgroundResource(android.R.drawable.btn_default)
        b4.setBackgroundResource(android.R.drawable.btn_default)
        b5.setBackgroundResource(android.R.drawable.btn_default)
        b6.setBackgroundResource(android.R.drawable.btn_default)
        b7.setBackgroundResource(android.R.drawable.btn_default)
        b8.setBackgroundResource(android.R.drawable.btn_default)
        b9.setBackgroundResource(android.R.drawable.btn_default)

        b1.text = ""
        b2.text = ""
        b3.text = ""
        b4.text = ""
        b5.text = ""
        b6.text = ""
        b7.text = ""
        b8.text = ""
        b9.text = ""

        b1.isEnabled = true
        b2.isEnabled = true
        b3.isEnabled = true
        b4.isEnabled = true
        b5.isEnabled = true
        b6.isEnabled = true
        b7.isEnabled = true
        b8.isEnabled = true
        b9.isEnabled = true

        player.clear()
        cpu.clear()
        currentPlayer = 1
    }

    fun buttonClick(view: View)
    {
        val buSelected: Button = view as Button
        var cellId = 0
        when(buSelected.id)
        {
            R.id.b1 -> cellId = 1
            R.id.b2 -> cellId = 2
            R.id.b3 -> cellId = 3
            R.id.b4 -> cellId = 4
            R.id.b5 -> cellId = 5
            R.id.b6 -> cellId = 6
            R.id.b7 -> cellId = 7
            R.id.b8 -> cellId = 8
            R.id.b9 -> cellId = 9
        }
        startGame(cellId,buSelected)
    }

    fun startGame(cellId:Int, buSelected:Button)
    {
        if (currentPlayer == 1)
        {
            buSelected.text = "+"
            buSelected.isEnabled = false
            buSelected.setBackgroundColor(Color.RED)
            player.add(cellId)
            currentPlayer = 2
            try {
                playMoveFromCpu()
            }catch (ex:Exception)
            {
                Toast.makeText(this,"Tie",Toast.LENGTH_SHORT).show()
                cleanBoard()
            }
        }
        else
        {
            buSelected.text = "+"
            buSelected.isEnabled = false
            buSelected.setBackgroundColor(Color.GREEN)
            cpu.add(cellId)
            currentPlayer = 1
        }
        getWinner()
    }

    fun getWinner()
    {
        var winner = -1
        if (player.contains(1) && player.contains(2) && player.contains(3))
        {
            winner = 1
        }
        if (cpu.contains(1) && cpu.contains(2) && cpu.contains(3))
        {
            winner = 2
        }
        if (player.contains(4) && player.contains(5) && player.contains(6))
        {
            winner = 1
        }
        if (cpu.contains(4) && cpu.contains(5) && cpu.contains(6))
        {
            winner = 2
        }

        if (player.contains(7) && player.contains(8) && player.contains(9))
        {
            winner = 1
        }
        if (cpu.contains(7) && cpu.contains(8) && cpu.contains(9))
        {
            winner = 2
        }

        if (player.contains(1) && player.contains(4) && player.contains(7))
        {
            winner = 1
        }
        if (cpu.contains(1) && cpu.contains(4) && cpu.contains(7))
        {
            winner = 2
        }

        if (player.contains(2) && player.contains(5) && player.contains(8))
        {
            winner = 1
        }
        if (cpu.contains(2) && cpu.contains(5) && cpu.contains(8))
        {
            winner = 2
        }

        if (player.contains(3) && player.contains(6) && player.contains(9))
        {
            winner = 1
        }
        if (cpu.contains(3) && cpu.contains(6) && cpu.contains(9))
        {
            winner = 2
        }
        if (player.contains(1) && player.contains(5) && player.contains(9))
        {
            winner = 1
        }
        if (cpu.contains(1) && cpu.contains(5) && cpu.contains(9))
        {
            winner = 2
        }
        if (player.contains(3) && player.contains(5) && player.contains(7))
        {
            winner = 1
        }
        if (cpu.contains(3) && cpu.contains(5) && cpu.contains(7))
        {
            winner = 2
        }

        if (winner != -1)
        {
            if (winner == 1)
            {
                alertWinner(winner)
            }
            else
            {
                alertWinner(winner)
            }
        }
    }


    fun alertWinner(winner: Int){
        val builder = AlertDialog.Builder(this)
        if(winner == 1) {
            builder.setTitle("Ganaste")
            builder.setMessage("Gracias por participar Joel")
        } else{
            builder.setTitle("Perdiste")
            builder.setMessage("Intentalo de nuevo")
        }
        builder.setPositiveButton("Restart"){ builder,_ ->
            cleanBoard()
            builder.cancel()
        }
        builder.show()
    }

    fun playMoveFromCpu()
    {
        val emptyCells = ArrayList<Int>()
        for (cellId in 1..9) {
            if (player.contains(cellId) || cpu.contains(cellId))
            {}
            else
            {
                emptyCells.add(cellId)
            }
        }

        val r = Random()
        val randomIndex = r.nextInt(emptyCells.size-0)+0
        val cellId = emptyCells[randomIndex]

        val buSelect:Button?
        when(cellId)
        {
            1 -> buSelect = b1
            2 -> buSelect = b2
            3 -> buSelect = b3
            4 -> buSelect = b4
            5 -> buSelect = b5
            6 -> buSelect = b6
            7 -> buSelect = b7
            8 -> buSelect = b8
            9 -> buSelect = b9
            else -> buSelect = b1
        }

        startGame(cellId,buSelect)
    }

}

